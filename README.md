# Template for backend course

Для запуска в виртуальном окружении необходимо проставить переменные окружения:
 TELEGRAM_BOT_TOKEN, SECRET_KEY

Запуск сервера: make dev

Запуск бота: make run_bot

Endpoint /me ждет в параметры telegram_id, как уникальный идентификатор пользователя
