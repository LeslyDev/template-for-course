from django.core.management.base import BaseCommand

from app.internal.bot import start_bot


class Command(BaseCommand):
    help = "Telega bot"

    def handle(self, *args, **options) -> None:
        start_bot()
