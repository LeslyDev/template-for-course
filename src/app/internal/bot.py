import logging

from django.conf import settings
from telegram.ext import CommandHandler, ConversationHandler, Filters, MessageHandler, Updater

from app.internal.transport.bot.handlers import cancel, get_me_info, save_phone, start, start_set_phone

logger = logging.getLogger(__name__)
logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)


def start_bot() -> None:
    updater = Updater(token=settings.TELEGRAM_BOT_TOKEN, use_context=True)
    logger.info("Bot started successfully, info: %s", updater.bot.get_me())

    start_handler = CommandHandler("start", start)

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("set_phone", start_set_phone)],
        states={
            0: [MessageHandler(Filters.text & (~Filters.command), save_phone)],
        },
        fallbacks=[CommandHandler("cancel", cancel)],
    )

    me_info_handler = CommandHandler("me", get_me_info)

    updater.dispatcher.add_handler(start_handler)
    updater.dispatcher.add_handler(conv_handler)
    updater.dispatcher.add_handler(me_info_handler)

    updater.start_polling()
    updater.idle()
