from typing import Union

from django.http import HttpRequest, HttpResponseNotFound, JsonResponse

from app.internal.services.user_service import get_user


def me_info(request: HttpRequest, telegram_id: int) -> Union[JsonResponse, HttpResponseNotFound]:
    user = get_user(telegram_id)
    if user is None:
        return HttpResponseNotFound("User not found")
    if not user.phone_number:
        return HttpResponseNotFound("Phone number not set")
    return JsonResponse(
        {
            "first_name": user.first_name,
            "last_name": user.last_name,
            "username": user.username,
            "phone number": user.phone_number,
        }
    )
