import enum
import logging
from functools import wraps

from telegram import Update
from telegram.ext import CallbackContext, ConversationHandler

from app.internal.services.user_service import get_or_create_user, get_user, save_phone_number
from app.internal.services.validators import is_correct_phone_number

logger = logging.getLogger(__name__)


class BotAnswers(str, enum.Enum):
    DO_START = "Сначала выполни команду /start"
    DO_SET_PHONE = "Сначала выполни команду /set_phone"
    SUCCESS_SAVE_INFO = "Информация о вас успешно сохранена"
    HELLO = "Привет {name}!"
    ENTER_PHONE_NUMBER = "Напиши свой номер телефона"
    INCORRECT_PHONE_NUMBER = "Введите корректный номер телефона или команду /cancel"
    PHONE_SUCCESS_SAVE = "Номер успешно записан"
    CANCEL = "Отменено"
    USER_INFO = "Привет, информация о тебе: \nЛогин: {username}, Номер телефона: {phone_number}"


def check_start(func):
    @wraps(func)
    def wrapper(update: Update, context: CallbackContext):
        user = get_user(update.effective_user.id)
        if user is None:
            update.message.reply_text(BotAnswers.DO_START)
            return None
        return func(update, context)

    return wrapper


def check_phone(func):
    @wraps(func)
    def wrapper(update: Update, context: CallbackContext):
        user = get_user(update.effective_user.id)
        if user.phone_number is None:
            update.message.reply_text(BotAnswers.DO_SET_PHONE)
            return None
        return func(update, context)

    return wrapper


def start(update: Update, context: CallbackContext) -> None:
    _, status = get_or_create_user(
        telegram_id=update.effective_user.id,
        first_name=update.effective_user.first_name,
        last_name=update.effective_user.last_name,
        username=update.effective_user.username,
    )
    if status:
        update.message.reply_text(BotAnswers.SUCCESS_SAVE_INFO)
    else:
        update.message.reply_text(BotAnswers.HELLO.format(name=update.effective_user.username.capitalize()))


@check_start
def start_set_phone(update: Update, context: CallbackContext) -> int:
    logger.info("Start save phone")
    update.message.reply_text(BotAnswers.ENTER_PHONE_NUMBER)

    return 0


def save_phone(update: Update, context: CallbackContext) -> int:
    logger.info("Try to save phone %s for user with username: %s", update.message.text, update.effective_user.username)

    if not is_correct_phone_number(update.message.text):
        update.message.reply_text(BotAnswers.INCORRECT_PHONE_NUMBER)
        return 0

    save_phone_number(update.effective_user.id, update.message.text)
    update.message.reply_text(BotAnswers.PHONE_SUCCESS_SAVE)

    return ConversationHandler.END


def cancel(update: Update, context: CallbackContext) -> int:
    """Cancels and ends the conversation."""
    logger.info("User %s canceled the conversation.", update.message.from_user.username)
    update.message.reply_text(BotAnswers.CANCEL)

    return ConversationHandler.END


@check_start
@check_phone
def get_me_info(update: Update, context: CallbackContext) -> None:
    user = get_user(update.effective_user.id)
    update.message.reply_text(BotAnswers.USER_INFO.format(username=user.username, phone_number=user.phone_number))
