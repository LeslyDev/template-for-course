from django.urls import path

from app.internal.transport.rest.handlers import me_info

urlpatterns = [
    path("me/<int:telegram_id>", me_info, name="me"),
]
