from django.core.validators import RegexValidator
from django.db import models


class User(models.Model):
    telegram_id = models.IntegerField(unique=True, db_index=True)
    first_name = models.CharField(max_length=32, null=True, verbose_name="Имя")
    last_name = models.CharField(max_length=32, null=True, verbose_name="Фамилия")
    username = models.CharField(max_length=20, verbose_name="Логин пользователя", null=True)
    phone_number_regex = RegexValidator(regex=r"^\+?1?\d{8,15}$")
    phone_number = models.CharField(
        validators=[phone_number_regex], max_length=16, unique=True, verbose_name="Номер телефона", null=True
    )

    class Meta:
        verbose_name = "Telegram user"
