from django.contrib import admin

from app.internal.models.user import User


@admin.register(User)
class AdminUser(admin.ModelAdmin):
    list_display = ("telegram_id", "username", "first_name", "last_name", "phone_number")
