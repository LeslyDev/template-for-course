import re

PHONE_RE = r"^\+?1?\d{8,15}$"
PHONE_COMPILE = re.compile(PHONE_RE)


def is_correct_phone_number(phone_number: str) -> bool:
    if PHONE_COMPILE.fullmatch(phone_number) is None:
        return False
    return True
