from typing import Optional

from django.core.exceptions import ObjectDoesNotExist

from app.internal.models.user import User


def get_or_create_user(telegram_id: int, username: str, first_name: str, last_name: str) -> tuple[User, bool]:
    return User.objects.get_or_create(
        telegram_id=telegram_id,
        username=username,
        first_name=first_name,
        last_name=last_name,
    )


def get_user(telegram_id: int) -> Optional[User]:
    try:
        return User.objects.get(telegram_id=telegram_id)
    except ObjectDoesNotExist:
        return None


def save_phone_number(telegram_id: int, phone_number: str) -> None:
    user = get_user(telegram_id)
    user.phone_number = phone_number
    user.save()
